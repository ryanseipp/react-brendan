import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import countButton from "./features/countButton/reducers/countButton";

const rootReducer = history => combineReducers({
  router: connectRouter(history),
  countButton,
});

export default rootReducer;