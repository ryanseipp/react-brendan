import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createRootReducer from '../rootReducer';

const PRODUCTION = process.env.NODE_ENV === 'production';

const createProductionStore = (history, middleware) =>
  createStore(
    createStore(createRootReducer(history)),
    applyMiddleware(...middleware)
  );

const createDevStore = (history, middleware) => {
  const composeWithDevTools = require('redux-devtools-extension')
    .composeWithDevTools;

  const store = createStore(
    createRootReducer(history),
    composeWithDevTools(applyMiddleware(...middleware))
  );

  if (module.hot) {
    module.hot.accept('../rootReducer', () => {
      store.replaceReducer(require('../rootReducer').default);
    });
  }

  return store;
};

export const configureStore = (history) => {
  const middleware = [
    thunkMiddleware,
    promiseMiddleware,
    routerMiddleware(history),
  ];

  return PRODUCTION
    ? createProductionStore(history, middleware)
    : createDevStore(history, middleware);
};
