import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {incrementCount} from '../actions/incrementCount';
import getCount from "../selectors/getCount";

const CountButton = () => {
  const dispatch = useDispatch();
  const handleClick = () => dispatch(incrementCount());
  const count = useSelector(getCount);

  return (
    <div>
      <p>{count}</p>
      <button onClick={handleClick}>Click me!</button>
    </div>
  )
};

export default CountButton;