import {INCREMENT_COUNT} from "../actions/incrementCount";

const initialState = {
  count: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT_COUNT:
      return {...state, count: state.count + 1};
    default:
      return state;
  }
};
