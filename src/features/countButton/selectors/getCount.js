import {createSelector} from 'reselect';

export default createSelector(
  state => state.countButton,
  countButton => countButton.count
);
