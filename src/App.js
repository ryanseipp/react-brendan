import React from 'react';
import logo from './logo.svg';
import './App.css';

import CountButton from "./features/countButton/components/CountButton";
import {Provider} from "react-redux";

function App({store}) {
  return (
    <Provider store={store}>
      <div className='App'>
        <header className='App-header'>
          <img src={logo} className='App-logo' alt='logo'/>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <CountButton/>
          <a
            className='App-link'
            href='https://reactjs.org'
            target='_blank'
            rel='noopener noreferrer'
          >
            Learn React
          </a>
        </header>
      </div>
    </Provider>
  );
}

export default App;
